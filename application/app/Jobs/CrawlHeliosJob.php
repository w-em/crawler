<?php

namespace App\Jobs;

use App\Models\CrawlResult;
use PHPHtmlParser\Dom;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CrawlHeliosJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 180;

    /**
     * @var null
     */
    protected $url = null;
    protected $data = null;

    /**
     * @var bool
     */
    protected $force = null;

    protected $contentMarkers = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($force = false)
    {
        $this->force = $force;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $where = [
            ['url', 'like', 'https://heliosselect.de/%']
        ];
        $query = CrawlResult::where($where);
        if ($this->force === false) {
            $query->whereNull('attributes');
        }

        $crawlResults = $query->get();

        foreach ($crawlResults as $crawlResult) {
            GenerateHeliosAttributeJob::dispatch($crawlResult);
        }
    }
}
