<?php

namespace App\Models;

class ImportContacts
{

    const ACCOUNT_ID = 9037772;

    protected $adresses;
    protected $relations;

    protected $countryTableID = 5354392;
    protected $countryTableName = 'Countries';
    protected $contactTableId = 5354394;
    protected $contactTableName = 'Contacts Sales';
    protected $contactRelationTableId = 5354416;
    protected $contactRelationTableName = 'Contact relations for Sales';

    function __construct()
    {
        $this->adresses = $this->getFromCsvFile(resource_path('import/contact_address.csv'));
        $this->relations = $this->getFromCsvFile(resource_path('import/contacts_all.csv'));

        $this->initialize();
    }

    protected function initialize()
    {
        // $contactTable = \HubSpot::hubdb()->getTable($this->contactTableId, self::ACCOUNT_ID, true);
        // $countryTable = \HubSpot::hubdb()->getTable($this->countryTableID, self::ACCOUNT_ID, true);


        // $this->updateCountryTableFromRelations();
        // $result = $this->updateContactTableFromAdresses();

        // $this->importContactsFromAdresses();
        // $result = $this->updateContactRelationTableFromAdresses();
        $result = $this->importContactRelationsFromAdresses();
        // dd($result);

    }

    /**
     * @return void
     */
    protected function importContactRelationsFromAdresses() {
        $countries = [];
        $accounts = [];
        $datas = [];
        $invalid = [];
        $table = \HubSpot::hubdb()->getTable($this->contactRelationTableId, self::ACCOUNT_ID, true);
        $accountsRows = \HubSpot::hubdb()->getRows($this->contactTableId, self::ACCOUNT_ID, true);

        foreach ($table->data->columns[0]->options as $c) {
            $countries[strtoupper($c->name)] = $c->id;
        }

        foreach ((array) $accountsRows->data->objects as $c) {
            $keys = array_keys((array) $c->values);
            $prop = $keys[2];
            $d = (array) $c->values;
            $accounts[strtoupper(trim($d[$prop]))] = $c->id;
        }

        $i = 0;
        foreach ($this->relations as $result) {

            // if ($i > 10) continue;
            if (strlen(trim($result['ACCOUNTSYNTEGON'])) > 0 && !key_exists(strtoupper($result['ACCOUNTSYNTEGON']), $accounts)) {
                if (!in_array($result['ACCOUNTSYNTEGON'], $invalid)) {
                    $invalid[] = $result['ACCOUNTSYNTEGON'];
                }
            }
            elseif (strlen(trim($result['ACCOUNTAGENT'])) > 0 && !key_exists(strtoupper($result['ACCOUNTAGENT']), $accounts)) {
                if (!in_array($result['ACCOUNTAGENT'], $invalid)) {
                    $invalid[] = $result['ACCOUNTAGENT'];
                }
            }
            else {
                if (strlen(trim($result['COUNTRYEN'])) === 0) {
                    dd($result);
                }

                $country = $this->getCountry($countries, strtoupper($result["COUNTRYEN"]));
                $datas[] = [
                    $country,
                    $accounts[strtoupper(trim($result['ACCOUNTSYNTEGON']))],
                    strlen(trim($result['ACCOUNTAGENT'])) > 0 ? $accounts[strtoupper($result['ACCOUNTAGENT'])] : null,
                    $result["BU"],
                    $result["LE"],
                    strlen(trim($result['ACCOUNTEMAILCC'])) > 0 ? $accounts[strtoupper($result['ACCOUNTEMAILCC'])] : null,
                    strlen(trim($result['ACCOUNTEMAILBCC'])) > 0 ? $accounts[strtoupper($result['ACCOUNTEMAILBCC'])] : null,
                    strlen(trim($result['AGENTEMAILCC'])) > 0 ? $accounts[strtoupper($result['AGENTEMAILCC'])] : null,
                    strlen(trim($result['AGENTEMAILBCC'])) > 0 ? $accounts[strtoupper($result['AGENTEMAILBCC'])] : null,
                ];
            }
            $i++;
        }

        // dump($invalid);
        return $this->importArrayToHubDB($datas, $this->contactRelationTableId);

    }

    protected function updateContactRelationTableFromAdresses()
    {

        $countries = $this->getCountriesFromRelations();

        $contactTable = \HubSpot::hubdb()->getTable($this->contactTableId, self::ACCOUNT_ID, true);
        $columnId = 26;

        foreach ($contactTable->data->columns as $column) {
            if ($column->name === 'key') {
                $columnId = $column->id;
            }
        }


        $options = [];
        foreach ($countries as $country) {
            $options[] = ["name" => $country['name'], "type" => "option"];
        }
        $columns = [
            [
                "name" => "country",
                "label" => "Country",
                "type" => "SELECT",
                "options" => $options
            ],
            [
                "name" => "account",
                "label" => "Account",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $this->contactTableId,
                "foreignColumnId" => $columnId
            ],
            [
                "name" => "account_agent",
                "label" => "Agent",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $this->contactTableId,
                "foreignColumnId" => $columnId
            ],
            [
                "label" => "BU",
                "name" => "bu",
                "type" => "SELECT",
                "options" => [
                    [ "name" => "PA-CF", "type" => "option"],
                    ["name" => "PA-PH", "type" => "option"],
                    ["name" => "PA-MT", "type" => "option"]
                ]
            ],
            [
                "label" => "LE",
                "name" => "le",
                "type" => "TEXT"
            ],
            [
                "label" => "Account cc",
                "name" => "account cc",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $this->contactTableId,
                "foreignColumnId" => $columnId
            ],
            [
                "label" => "Account_bcc",
                "name" => "account_bcc",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $this->contactTableId,
                "foreignColumnId" => $columnId
            ],
            [
                "label" => "Agent_cc",
                "name" => "agent_cc",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $this->contactTableId,
                "foreignColumnId" => $columnId
            ],
            [
                "label" => "Agent_bcc",
                "name" => "agent_bcc",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $this->contactTableId,
                "foreignColumnId" => $columnId
            ]
        ];


        return \HubSpot::hubdb()->updateTable($this->contactRelationTableId, $this->contactRelationTableName, $columns, $draft = false, $published = true, $useForPages = false);
    }

    /**
     * @return void
     */
    protected function importContactsFromAdresses() {
        $table = \HubSpot::hubdb()->getTable($this->contactTableId, self::ACCOUNT_ID, true);
        $countries = [];

        foreach ($table->data->columns[0]->options as $c) {
            $countries[strtoupper($c->name)] = $c->id;
        }

        $datas = [];
        foreach ($this->adresses as $relation) {
            $datas[] = [
                $this->getCountry($countries, strtoupper($relation["COUNTRY"])),
                $relation["COMPANYSORT"],
                trim($relation["ACCOUNT"]),
                trim($relation["ACCOUNTNAME1"]),
                $relation["TELEPHONE1"],
                trim($relation["EMAIL1"])
            ];
        }

        $this->importArrayToHubDB($datas, $this->contactTableId);

    }

    /**
     * @param $countries
     * @param $country
     * @return mixed
     */
    protected function getCountry($countries, $country) {

        if($country === 'BOSNIA HERZ') $country = strtoupper('Bosnia and Herzegovina');
        if($country === 'CHINA P. R.') $country = strtoupper('China R. P.');
        if($country === 'CHINA') $country = strtoupper('China R. P.');
        if($country === 'USA') $country = strtoupper('United States of America');
        if($country === 'EMEA') $country = strtoupper('Switzerland');
        if($country === 'ISLAND') $country = strtoupper('Iceland');
        if($country === 'SCHWEDEN') $country = strtoupper('Sweden');
        if($country === 'DOM. REPUBLIC') $country = strtoupper('Dominican Republic');
        if($country === 'BANGLADESH P. R.') $country = strtoupper('BANGLADESH');
        if($country === 'RUSSIAN FEDERAT') $country = strtoupper('Russian Federation');
        if($country === 'RUSSIA') $country = strtoupper('Russian Federation');
        if($country === 'BELGIA') $country = strtoupper('Belgium');
        if($country === 'CHZECH') $country = strtoupper('Czechia');
        if($country === 'CZECH REPUBLIC') $country = strtoupper('Czechia');
        if($country === 'HUNGARIA') $country = strtoupper('Hungary');
        if($country === 'ROMANIA') $country = strtoupper('Rumania');
        if($country === 'RUANDA') $country = strtoupper('Rwanda');


        return $countries[$country];
    }

    /**
     * @return void
     */
    protected function updateContactTableFromAdresses()
    {

        $countries = $this->getCountriesFromRelations();
        $options = [];
        foreach ($countries as $country) {
            $options[] = ["name" => $country['name'], "type" => "option"];
        }
        $columns = [
            [
                "name" => "country",
                "label" => "Country",
                "type" => "SELECT",
                "options" => $options
            ],
            [
                "name" => "company_sort",
                "label" => "Company Sort",
                "type" => "TEXT"
            ],
            [
                "name" => "key",
                "label" => "Account Key",
                "type" => "TEXT"
            ],

            [
                "name" => "name",
                "label" => "Account name",
                "type" => "TEXT"
            ],
            [
                "label" => "Phone",
                "name" => "phone",
                "type" => "TEXT"
            ],
            [
                "label" => "Email",
                "name" => "email",
                "type" => "TEXT"
            ]
        ];


        $updateResult = \HubSpot::hubdb()->updateTable($this->contactTableId, $this->contactTableName, $columns, $draft = false, $published = true, $useForPages = false);
    }

    /**
     * get world wide regions
     * @return array
     */
    protected function getCountriesFromRelations()
    {
        $regions = $this->getRegionsFromRelations();
        foreach ($this->relations as $relation) {
            $region = $relation["REGION"];
            $countries[$relation["ALPHA2"]] = [
                "region" => array_search($region, $regions) + 1,
                "alpha2" => $relation["ALPHA2"],
                "name" => $relation["COUNTRYEN"]
            ];
        }

        return array_values($countries);
    }

    /**
     * get world wide regions
     * @return array
     */
    protected function getRegionsFromRelations()
    {
        $regionOptions = [];
        foreach ($this->relations as $relation) {
            $region = $relation["REGION"];
            if (!in_array($region, $regionOptions)) {
                $regionOptions[] = $region;

            }
        }
        sort($regionOptions);

        return $regionOptions;
    }

    /**
     * @param $updateTable
     * @return void
     */
    protected function updateCountryTableFromRelations($updateTable = false)
    {
        if ($updateTable) {
            $regions = $this->getRegionsFromRelations();
            $regionOptions = [];

            foreach ($regions as $region) {
                $regionOptions[] = ["name" => $region, "type" => "option"];
            }

            $countryColumns = $this->getCountryColumns();
            $countryColumns[0]['options'] = $regionOptions;

            $updateResult = \HubSpot::hubdb()->updateTable($this->countryTableID, $this->countryTableName, $countryColumns, $draft = false, $published = true, $useForPages = false);
        }

        $countries = $this->getCountriesFromRelations();

        $this->importArrayToHubDB($countries, $this->countryTableID);
    }

    /**
     * @return array
     */
    protected function getCountryColumns()
    {
        return [
            [
                "name" => "Region",
                "type" => "SELECT",
                "options" => []
            ],
            [
                "name" => "Name",
                "type" => "TEXT"
            ],
            [
                "name" => "Alpha",
                "type" => "TEXT"
            ]
        ];
    }

    /**
     * @param array $data
     * @param $tableId
     * @return void
     */
    protected function importArrayToHubDB(array $data, $tableId) {
        $table = \HubSpot::hubdb()->getTable($tableId, self::ACCOUNT_ID, true);

        $columnMappings = [];
        for ($i = 0; $i < count($data[0]); $i++) {
            $columnMappings[] = [
                "source" => $i + 1,
                "target" => $table->data->columns[$i]->id,
            ];
        }

        $tempFileName = '/tmp/temptest.csv';
        $tempFile = fopen($tempFileName, 'w');
        foreach ($data as $line) {
            fputcsv($tempFile, $line, ';');
        }
        fclose($tempFile);

        $config = [
            "resetTable" => true,
            "skipRows" => 0,
            "format" => "csv",
            "separator" => ";",
            "columnMappings" => $columnMappings
        ];
        $updateResult = \HubSpot::hubdb()->import($tableId, $tempFileName, $config);
        // $unpublishResult = \HubSpot::hubdb()->revertDraftTable($tableId);
        // $publishResult = \HubSpot::hubdb()->publishDraftTable($tableId);

        return $updateResult;
    }

    protected function publishTable($tableId, $publish = true) {
        if ($publish) return \HubSpot::hubdb()->publishDraftTable($tableId);
        else return \HubSpot::hubdb()->revertDraftTable($tableId);
    }

    /**
     * @param $file
     * @return array
     * @throws \Exception
     */
    protected function getFromCsvFile($file)
    {
        $rows = array_map(function ($data) {
            return str_getcsv($data, ";");
        }, file($file));
        $header = array_shift($rows);
        foreach ($header as $key => $value) {
            $header[$key] = strtoupper(trim(preg_replace('/[^A-Za-z0-9\-]/', '', $value)));
        }

        $csv = array();
        foreach ($rows as $row) {
            try {
                $csv[] = array_combine($header, $row);
            } catch (\Exception $e) {
                throw $e;
                dd($row);
            }

        }
        return $csv;
    }
}
