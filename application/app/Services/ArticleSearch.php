<?php

namespace App\Services;

use App\Models\Article;
use App\Models\ArticleAttribute;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;
use TeamTNT\TNTSearch\TNTSearch;

class ArticleSearch {

    const QUERY_CATEGORY = 'categories';
    const QUERY_OPERATOR = 'operator';

    /**
     * @var string
     */
    protected $term = '';

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * @var Model
     */
    protected $model;

    protected $articleUidResultList = [];

    protected $result = [];


    /**
     * @param $term
     * @param $filters
     */
    function __construct(string $term, array $filters) {
        $this->term = trim(strip_tags($term));
        $this->filters = $filters;
        $this->model = Article::class;
    }

    public function search() {
        $this->articleUidResultList['basic'] = $this->getBasicArticleUids()->toArray();
        $finalArticleIds = $this->filterAttributes();

        $this->result = $finalArticleIds;
    }

    public function getFilters () {
        // if (count($this->filters) === 0) return []; // no filters if no params given

        $results = [];

        if (key_exists('basic', $this->articleUidResultList)) {
            $results = $this->getFiltersByArticleIds($this->articleUidResultList['basic']);
        }
        /*
        foreach ($this->articleUidResultList as $key => $articleIds) {
            $results = array_merge_recursive($results, $this->getFiltersByArticleIds($articleIds));
        }
        */
        return $results;
    }

    public function getResult() {
        return $this->result;
    }

    // TODO: better performance
    protected function getFiltersByArticleIds($articleIds) {
        $articleAttributeQuery = ArticleAttribute::select('*')
            ->selectRaw('count(*) as total')
            ->whereIn('article_id', $articleIds)
            ->groupBy('attribute_id', 'attribute_value_id');

        $defaultFiltersIds = Attribute::where([
            ['standard_filter', '=', 1]
        ])->get()->pluck('id')->toArray();

        $map = [];
        foreach ($articleAttributeQuery->get() as $item) {
            if (!key_exists($item->attribute_id, $map)) {

                if (!in_array($item->attribute_id, $defaultFiltersIds)) {
                    continue;
                }

                $attribute = Attribute::findOrFail($item->attribute_id);
                $map[$item->attribute_id] = [
                    'id' => $item->attribute_id,
                    'label' => $attribute->title,
                    'slug' => $attribute->slug,
                    'optionIds' => []
                ];
            }

            $map[$item->attribute_id]['optionIds'][] = [
                'value_id' => $item->attribute_value_id,
                'total' => $item->total,
            ];

        }

        $filters = [];
        foreach ($map as $key => $mapItem) {

            if (count($mapItem['optionIds']) <= 1) { continue; }

            $filter = $mapItem;
            $optionIds = $mapItem['optionIds'];
            unset($filter['optionIds']);
            $filterOptions = [];
            $sortingArr = array();
            foreach ($optionIds as $optionId) {
                $attributeValue = AttributeValue::findOrFail($optionId['value_id']);
                $filterOptions[$attributeValue->value] = [
                    'label' => $attributeValue->value,
                    'slug' => $attributeValue->value,
                ];

                $sortingArr[$attributeValue->value] = $attributeValue->value;

            }

            array_multisort($sortingArr, SORT_ASC, $filterOptions);

            $filter['options'] = array_values($filterOptions);
            $filters[] = $filter;
        }

        return $filters;
    }

    protected function filterAttributes () {

        $articleIds = $this->articleUidResultList['basic'];

        foreach ($this->filters as $key => $filter) {
            if (!key_exists('name', $filter) || !key_exists('values', $filter) || !is_array($filter['values']) || $filter['name'] === self::QUERY_CATEGORY) {
                continue;
            }
            $filterName = strtolower(trim(strip_tags($filter['name'])));

            $articleIds = $this->articleUidResultList['filter_' . $filterName] = $this->getFilteredArticleIds($filter, $articleIds);
        }

        return $articleIds;
    }

    protected function getFilteredArticleIds($filter, $articleIds) {
        $attribute = Attribute::where([['slug', '=', $filter['name']]])->first();

        if (!$attribute) { return $articleIds; }

        $questionaries = substr(str_repeat('?,', count($filter['values'])), 0, -1);
        $questionaries2 = substr(str_repeat('?,', count($articleIds)), 0, -1);

        $sql = "SELECT group_concat(DISTINCT a.id) AS article_ids FROM articles a
                    LEFT JOIN article_attributes AS attr ON (attr.article_id = a.id)
                    WHERE (
                        attr.attribute_id = ? AND attr.attribute_value_id IN (select id from attribute_values where attribute_id = ? and value IN($questionaries))
                    ) and a.id in ($questionaries2)";

        $data = array_merge([$attribute->id, $attribute->id], $filter['values'], $articleIds);

        try {
            $result = DB::select($sql, $data);

            if (count($result) > 0) {
                if (!empty(trim($result[0]->article_ids))) {
                    $articleIds = array_map('intval', explode(',', $result[0]->article_ids) );

                    return $articleIds;
                }
            }
        } catch (\Exception $exception) {
            return [];
        }


        return [];

    }

    protected function getBasicArticleUids() {
        // categories
        $categoryIdResult = $this->getCategoryIdResultFromQuery($this->filters);
        if (count($categoryIdResult['ids']) > 0) {
            $query = $this->model::whereIn('id', function ($query) use ($categoryIdResult) {
                $query->select('article_id')->from('article_categories');

                if ($categoryIdResult[self::QUERY_OPERATOR] === 'or') {
                    $query->whereIn('category_id', $categoryIdResult['ids']);
                }
                else { // TODO: and query
                    $query->whereIn('category_id', $categoryIdResult['ids']);
                }

            });
        } else {
            $query = new $this->model;
        }

        if ($this->hasValidTerm()) {
            $query = Article::search($this->term)->constrain($query);
        } else {
            $query = $query->select('id');
        }

        return $query->get()->pluck('id');
    }

    /**
     * extract categories from query and return array of categoryIds
     * @param $queries
     * @return array
     */
    protected function getCategoryIdResultFromQuery($queries) {

        $categoryIds = [
            self::QUERY_OPERATOR => 'and',
            'ids' => []
        ];

        $categoryFilter = collect($queries)->filter(function ($obj) {
            if ($obj['name'] === self::QUERY_CATEGORY) {
                return $obj;
            }
        })->first();

        if (is_null($categoryFilter) || !key_exists('values', $categoryFilter)) return $categoryIds;

        $categorySlugs = [];
        foreach ($categoryFilter['values'] as $value) {
            $categorySlugs[] = $value;
        }

        if (count($categorySlugs) > 0) {
            $categoryIds = Category::whereIn('slug', $categorySlugs)->get()->pluck('id');
        }
        return [
            self::QUERY_OPERATOR => $categoryFilter[self::QUERY_OPERATOR],
            'ids' => $categoryIds
        ];
    }

    /**
     * check the search term is valid
     * @return bool
     */
    protected function hasValidTerm() {
        return !empty($this->term);
    }
}
