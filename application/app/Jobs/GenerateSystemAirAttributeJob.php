<?php

namespace App\Jobs;

use App\Models\CrawlResult;
use PHPHtmlParser\Dom;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateSystemAirAttributeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 180;

    /**
     * @var null
     */
    protected $data = null;

    /**
     * @var bool
     */
    protected $crawlResult = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CrawlResult $crawlResult)
    {
        $this->crawlResult = $crawlResult;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $content = $this->crawlResult->html_content;
        $dom = new Dom;
        $dom->loadStr($content);

        $title = trim(strip_tags((string) $dom->find('h1.product-detail-info-headline')[0]));
        $articleNumber = str_replace('Artikelnummer:', '', trim(strip_tags((string) $dom->find('p.product-detail-info-artno')[0])));
        $descriptionLines = $dom->find('div.product-detail-info .item-description')[0];
        $imgUrl = $dom->find('a.ecom-productimage img')[0];
        $imgUrl = $imgUrl->getTag()->getAttribute('src')->getValue();


        // dd((string) $dom->find('.product-detail-footer-price')[0]);
        // $price = trim(strip_tags((string) $dom->find('.product-detail-footer-price')[0]));

        $descArr = [];
        foreach ($descriptionLines->getChildren() as $child) {
            $tag = $child->getTag();

            if (in_array($tag->name(), ['text', 'textarea', 'div'])) {
                continue;
            }
            $descArr[] = (string) $child;

        }

        $description = implode("", $descArr);
        $description=preg_replace('/class=".*?"/', '', $description);


        $this->crawlResult->update([
            'attributes' => [
                'article_no' => $articleNumber,
                'title' => $title,
                'image' => $imgUrl,
                'description' => $description
            ]
        ]);
    }
}
