<?php

namespace App\Services;

use App\Models\Article;
use App\Models\ArticleAttribute;
use App\Models\ArticleCategory;
use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\File;
use App\Models\FileReference;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ContactImportService
{

    public static function importContactRelations ($tableId, $accountTableId) {
        $results = self::getFromCsvFile(resource_path('import/contacts_all.csv'));
        $table = \HubSpot::hubdb()->getTable($tableId, 9037772, true);

        $accountsRows = \HubSpot::hubdb()->getRows($accountTableId, 9037772, true);
        $countryColumns = $table->data->columns[0]->foreignIds;

        $countries = [];
        $accounts = [];
        foreach ((array) $accountsRows->data->objects as $c) {
            $d = (array) $c->values;
            $accounts[strtoupper($d['3'])] = $c->id;
        }

        foreach ($countryColumns as $c) {
            $countries[strtoupper($c->name)] = $c->id;
        }

        $datas = [];
        $invalid = [];
        foreach ($results as $result) {

            if (strlen(trim($result['ACCOUNTSYNTEGON'])) > 0 && !key_exists(strtoupper($result['ACCOUNTSYNTEGON']), $accounts)) {
                if (!in_array($result['ACCOUNTSYNTEGON'], $invalid)) {
                    $invalid[] = $result['ACCOUNTSYNTEGON'];
                }
            }
            elseif (strlen(trim($result['ACCOUNTAGENT'])) > 0 && !key_exists(strtoupper($result['ACCOUNTAGENT']), $accounts)) {
                if (!in_array($result['ACCOUNTAGENT'], $invalid)) {
                    $invalid[] = $result['ACCOUNTAGENT'];
                }
            }
            else {
                if (strlen(trim($result['COUNTRYEN'])) === 0) {
                    dd($result);
                }
                $datas[] = [
                    $countries[strtoupper($result["COUNTRYEN"])],
                    $accounts[strtoupper($result['ACCOUNTSYNTEGON'])],
                    strlen(trim($result['ACCOUNTAGENT'])) > 0 ? $accounts[strtoupper($result['ACCOUNTAGENT'])] : null,
                    $result["BU"],
                    $result["LE"]
                ];
            }

        }

        dump($invalid);

        $temp_file = '/tmp/temptest.csv';

        self::convertToCSV($datas, $temp_file);

        $config = [
            "resetTable" => true,
            "skipRows" => 0,
            "format" => "csv",
            "separator" => ";",
            "columnMappings" => [
                [
                    "source" => 1,
                    "target" => 1
                ],
                [
                    "source" => 2,
                    "target" => 2
                ],
                [
                    "source" => 3,
                    "target" => 3
                ],
                [
                    "source" => 4,
                    "target" => 4
                ],
                [
                    "source" => 5,
                    "target" => 5
                ]
            ]
        ];
        $result = \HubSpot::hubdb()->import($tableId, $temp_file, $config);
dd($result);
        unlink($temp_file);

    }

    public static function createContactRelationTable($countryTableId, $accountTableId) {
        $columns = [
            [
                "name" => "country",
                "label" => "Country",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $countryTableId,
                "foreignColumnId" => 2
            ],
            [
                "name" => "account",
                "label" => "Account",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $accountTableId,
                "foreignColumnId" => 4
            ],
            [
                "name" => "account_agent",
                "label" => "Agent",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $accountTableId,
                "foreignColumnId" => 4
            ],
            [
                "label" => "BU",
                "name" => "bu",
                "type" => "SELECT",
                "options" => [
                    [ "name" => "PA-CF", "type" => "option"],
                    ["name" => "PA-PH", "type" => "option"],
                    ["name" => "PA-MT", "type" => "option"]
                ]
            ],
            [
                "label" => "LE",
                "name" => "le",
                "type" => "TEXT"
            ]
        ];

        return \HubSpot::hubdb()->createTable('Contact relations for Sales New', $columns);
    }

    public static function getCountry($countries, $country) {

        if($country === 'BOSNIA HERZ') $country = strtoupper('Bosnia and Herzegovina');
        if($country === 'CHINA P. R.') $country = strtoupper('China R. P.');
        if($country === 'CHINA') $country = strtoupper('China R. P.');
        if($country === 'USA') $country = strtoupper('United States of America');
        if($country === 'EMEA') $country = strtoupper('Switzerland');
        if($country === 'ISLAND') $country = strtoupper('Iceland');
        if($country === 'SCHWEDEN') $country = strtoupper('Sweden');
        if($country === 'DOM. REPUBLIC') $country = strtoupper('Dominican Republic');
        if($country === 'BANGLADESH P. R.') $country = strtoupper('BANGLADESH');
        if($country === 'RUSSIAN FEDERAT') $country = strtoupper('Russian Federation');
        if($country === 'RUSSIA') $country = strtoupper('Russian Federation');
        if($country === 'BELGIA') $country = strtoupper('Belgium');
        if($country === 'CHZECH') $country = strtoupper('Czechia');
        if($country === 'CZECH REPUBLIC') $country = strtoupper('Czechia');
        if($country === 'HUNGARIA') $country = strtoupper('Hungary');
        if($country === 'ROMANIA') $country = strtoupper('Rumania');


        return $countries[$country];
    }

    public static function importContacts ($tableId) {
        $results = self::getFromCsvFile(resource_path('import/contacts_address.csv'));
        $table = \HubSpot::hubdb()->getTable($tableId, 9037772, true);
        $temp_file = '/tmp/temptest.csv';
        $countries = [];

        $t = $table->data->columns[0]->foreignIds;
        foreach ($t as $c) {
            $countries[strtoupper($c->name)] = $c->id;
        }

        $datas = [];
        foreach ($results as $result) {
            $datas[] = [
                self::getCountry($countries, strtoupper($result["COUNTRY"])),
                $result["COMPANYSORT"],
                $result["ACCOUNT"],
                $result["ACCOUNTNAME1"],
                $result["TELEPHONE1"],
                $result["EMAIL1"]
            ];
        }

        self::convertToCSV($datas, $temp_file);

        $config = [
            "resetTable" => true,
            "skipRows" => 1,
            "format" => "csv",
            "separator" => ";",
            "columnMappings" => [
                [
                    "source" => 1,
                    "target" => 1
                ],
                [
                    "source" => 2,
                    "target" => 2
                ],
                [
                    "source" => 3,
                    "target" => 3
                ],
                [
                    "source" => 4,
                    "target" => 4
                ],
                [
                    "source" => 5,
                    "target" => 5
                ],
                [
                    "source" => 6,
                    "target" => 6
                ]
            ]
        ];
        \HubSpot::hubdb()->import($tableId, $temp_file, $config);

        unlink($temp_file);
    }

    public static  function createContactTable ($countryTableId) {
        $columns = [
            [
                "name" => "country",
                "label" => "Country",
                "type" => "FOREIGN_ID",
                "foreignTableId" => $countryTableId,
                "foreignColumnId" => 2
            ],
            [
                "name" => "company_sort",
                "label" => "Company Sort",
                "type" => "TEXT"
            ],
            [
                "name" => "key",
                "label" => "Account Key",
                "type" => "TEXT"
            ],

            [
                "name" => "name",
                "label" => "Account name",
                "type" => "TEXT"
            ],
            [
                "label" => "Phone",
                "name" => "phone",
                "type" => "TEXT"
            ],
            [
                "label" => "Email",
                "name" => "email",
                "type" => "TEXT"
            ]
        ];

        return \HubSpot::hubdb()->createTable('Contacts Sales Neu', $columns);
    }

    public static function createCountryTable() {
        $results = self::getFromCsvFile(resource_path('import/contacts_all.csv'));
        $countries = [];
        $regionOptions = [];
        $regionOptionsTemp = [];
        foreach ($results as $result) {
            $region = $result["REGION"];
            $countries[$result["ALPHA2"]] = [
                "region" => $region,
                "alpha2" => $result["ALPHA2"],
                "name" => $result["COUNTRYEN"]
            ];

            if (!in_array($region, $regionOptionsTemp)) {
                $regionOptionsTemp[] = $region;
                $regionOptions[] = ["name" => $region, "type" => "option"];
            }
        }

        $columns = [
            [
                "name" => "Region",
                "type" => "SELECT",
                "options" => $regionOptions
            ],
            [
                "name" => "Name",
                "type" => "TEXT"
            ],
            [
                "name" => "Alpha",
                "type" => "TEXT"
            ]
        ];

        return \HubSpot::hubdb()->createTable('Countries Neu', $columns);
    }

    public static function importCountries ($tableId)
    {
        $results = self::getFromCsvFile(resource_path('import/contacts_all.csv'));
        $countries = [];
        $regionOptionsTemp = [];
        foreach ($results as $result) {
            $region = $result["REGION"];
            if (!in_array($region, $regionOptionsTemp)) {
                $regionOptionsTemp[] = $region;
            }
        }

        foreach ($results as $result) {
            $region = $result["REGION"];
            $countries[$result["ALPHA2"]] = [
                "region" => array_search($region, $regionOptionsTemp) + 1,
                "alpha2" => $result["ALPHA2"],
                "name" => $result["COUNTRYEN"]
            ];
        }

        foreach($countries as $key => $country) {
            \HubSpot::hubdb()->addRow($tableId, [
                "1" => $country["region"],
                "2" => $country["name"],
                "3" => $country["alpha2"]
            ]);
        }
    }

    public static function getFromCsvFile($file)
    {
        $rows   = array_map(function($data) { return str_getcsv($data,";");}, file($file));
        $header = array_shift($rows);
        foreach ($header as $key => $value) {
            $header[$key] = strtoupper(trim(preg_replace('/[^A-Za-z0-9\-]/', '', $value)));
        }
        $csv    = array();
        foreach($rows as $row) {
            try {
                $csv[] = array_combine($header, $row);
            } catch (\Exception $e) {
                // throw $e;
                dd($row);
            }

        }
        return $csv;
    }

    public static function convertToCSV($input_array, $output_file) {
        $temp_file = fopen($output_file,'w');
        foreach ($input_array as $line) {
            fputcsv($temp_file, $line, ';');
        }
        fclose($temp_file);
    }

}
