<?php

namespace App\Jobs;

use App\Models\CrawlResult;
use PHPHtmlParser\Dom;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CrawlSystemAirSitemapJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 180;

    /**
     * @var null
     */
    protected $url = null;
    protected $data = null;

    /**
     * @var int
     */
    protected $pages = 1;

    protected $contentMarkers = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($pages)
    {
        $this->pages = $pages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $urls = [];
        for ($i = 1; $i <= $this->pages; $i++) {
            $url = 'https://shop.systemair.com/sitemap/sitemap_product_' . $i . '.xml';

            // $subDom = new Dom;
            // $subDom->loadFromUrl($url);
            $content = file_get_contents($url);
            $re = '/<xhtml:link rel="alternate" hreflang="de-DE" href="(.*)" \/>/m';

            preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);

            foreach ($matches as $match) {
                if(!in_array($match[1], $urls)) {
                    $urls[] = $match[1];
                }
            }
        }
        foreach($urls as $url) {
            CrawlJob::dispatch($url, $this->force);
        }
    }
}
