<?php

namespace App\Jobs;

use App\Models\CrawlResult;
use PHPHtmlParser\Dom;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateHeliosAttributeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 180;

    /**
     * @var null
     */
    protected $data = null;

    /**
     * @var bool
     */
    protected $crawlResult = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CrawlResult $crawlResult)
    {
        $this->crawlResult = $crawlResult;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $content = $this->crawlResult->html_content;

        $dom = new Dom;
        $dom->loadStr($content);
        $descArr = [];
        $titleArr = [];

        $tabs = $dom->find('#tabs li a');

        $articleNumber = (string) strip_tags($dom->find('.head')[0]);
        $articleNumberSplit = explode('-', $articleNumber);

        $articleNumber = trim(str_replace('Artikel: ', '', $articleNumberSplit[0]));

        foreach ($tabs as $tab) {
            $url = 'https://heliosselect.de/cms/' . $tab->getTag()->getAttribute('href')->getValue();
            // dump($url);
            $subDom = new Dom;
            $subDom->loadFromUrl($url);
            if (strpos($url, 'show=bild') > -1) {
                $tabContent = $subDom->find('fieldset.tab');
                $imgUrl = '';
                try {
                    if (count($tabContent) > 0) {
                        $imgUrl = $tabContent[0]->find('img')->getTag()->getAttribute('src')->getValue();
                    }

                } catch (\Exception $exception) {}

                $index = 0;
                foreach ($tabContent[1]->find('table td') as $td) {
                    foreach ($td->find('*') as $su) {
                        if ($index === 0 || $index === 1) {
                            $titleArr[] = trim(strip_tags((string) $su));
                        }
                        else {
                            $descArr[] = trim(strip_tags((string) $su));
                        }
                    }
                    $index++;
                }
            }
        }

        $title = implode(' ', $titleArr);
        $description = '<p>' . implode(' ', $descArr) . '</p>';

        $this->crawlResult->update([
            'attributes' => [
                'article_no' => $articleNumber,
                'title' => $title,
                'main_title' => $titleArr[0],
                'sub_title' => $titleArr[1],
                'image' => $imgUrl,
                'description' => $description
            ]
        ]);
    }
}
