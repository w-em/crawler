<?php

namespace App\Services;

use App\Models\Article;
use App\Models\ArticleAttribute;
use App\Models\ArticleCategory;
use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\File;
use App\Models\FileReference;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class JobImportService
{
    const TABLE_ID = 5334438;

    public static function import() {

        $results = self::loadAllJobs();

        $datas = [];
        $mapping = [];
        $index = 0;

        foreach ($results as $result) {
            $postalCode = '';
            $functionalArea = '';
            $function = $result->function->label;
            $countryName = $result->location->country;
            $department = '';
            try {$postalCode = $result->location->postalCode; } catch (\Exception $exception) {}

            if ($result->customField) {
                foreach ($result->customField as $attr) {
                    if ($attr->fieldId === 'COUNTRY') {
                        $countryName = $attr->valueLabel;
                    }

                    if ($attr->fieldLabel === 'Department.') {
                        $department = $attr->valueLabel;
                    }

                    if ($attr->fieldLabel === 'Functional Area') {
                        $functionalArea = $attr->valueLabel;
                    }
                }
            }

            $data = [
                $result->id,
                $result->name,
                $result->experienceLevel->label,
                $result->location->remote === false ? 0 : 1,
                $result->typeOfEmployment->label,
                $department,
                $functionalArea,
                $function,
                'https://jobs.smartrecruiters.com/SYNTEGON/' . $result->id,
                $result->company->name,
                $countryName,
                $result->location->country,
                $postalCode,
                $result->location->city,
                $result->location->address,
                $postalCode . ' ' . $result->location->city . ', ' . $result->location->address,
            ];
            $datas[] = $data;

            if ($index === 0) {
                foreach ($data as $key => $d) {
                    $mapping[] =  ["source" => $key + 1, "target" => $key + 1];
                }
            }

            $index++;
        }

        $config = [
            "resetTable" => true,
            "skipRows" => 0,
            "format" => "csv",
            "separator" => ";",
            "columnMappings" => $mapping
        ];

        $temp_file = '/tmp/temptest.csv';

        self::convertToCSV($datas, $temp_file);

        $updateCsvResult = \HubSpot::hubdb()->import(self::TABLE_ID, $temp_file, $config);
        unlink($temp_file);
    }

    public static function loadAllJobs() {
        $results = [];
        $loading = true;
        $offset = 0;
        $defaultUrl = "https://api.smartrecruiters.com/v1/companies/syntegon/postings?offset=";
        // https://api.smartrecruiters.com/v1/companies/syntegon/postings?offset=99

        while($loading) {
            $response = self::get($defaultUrl . $offset);

            if (count($response->content) === 0) {
                $loading = false;
            }

            $results = array_merge($results, $response->content);

            $offset = $offset + 100;
        }

        return $results;
    }

    /**
     * @param $input_array
     * @param $output_file
     */
    public static function convertToCSV($input_array, $output_file) {
        $temp_file = fopen($output_file,'w');
        foreach ($input_array as $line) {
            fputcsv($temp_file, $line, ';');
        }
        fclose($temp_file);
    }

    public static function createJobOfferTable () {
        $columns = [
            [
                "label" => "Code",
                "name" => "code",
                "type" => "TEXT"
            ],
            [
                "label" => "Job Title",
                "name" => "title",
                "type" => "TEXT"
            ],
            [
                "name" => "experience_level",
                "label" => "Experience Level",
                "type" => "TEXT"
            ],
            [
                "label" => "Remote",
                "name" => "remote",
                "type" => "BOOLEAN"
            ],
            [
                "label" => "Employment",
                "name" => "employment",
                "type" => "TEXT"
            ],
            [
                "label" => "Department",
                "name" => "department",
                "type" => "TEXT"
            ],
            [
                "label" => "Functional Area",
                "name" => "functional_area",
                "type" => "TEXT"
            ],
            [
                "label" => "Function",
                "name" => "function",
                "type" => "TEXT"
            ],
            [
                "name" => "url",
                "label" => "URL",
                "type" => "TEXT"
            ],
            [
                "name" => "company",
                "label" => "Company",
                "type" => "TEXT"
            ],
            [
                "name" => "country",
                "label" => "Country",
                "type" => "TEXT"
            ],
            [
                "name" => "country_iso",
                "label" => "Country ISO",
                "type" => "TEXT"
            ],
            [
                "name" => "zip",
                "label" => "ZIP",
                "type" => "TEXT"
            ],
            [
                "name" => "city",
                "label" => "city",
                "type" => "TEXT"
            ],
            [
                "name" => "address",
                "label" => "Address",
                "type" => "TEXT"
            ],
            [
                "name" => "location_string",
                "label" => "Location as Text",
                "type" => "TEXT"
            ]
        ];

        return \HubSpot::hubdb()->createTable('Dynamic Job Offers', $columns);
    }

    /**
     * @param $url
     * @return array|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function get($url) {

        $options = [
            // Additional headers for this specific request
            'headers' => [
                'Cache-Control' => 'no-cache',
                'Content-Type' => 'application/json'
            ],
            'allow_redirects' => false,
        ];

        $client = new \GuzzleHttp\Client($options);
        $response = $client->request('GET', $url, $options);
        return json_decode($response->getBody()->getContents());
    }
}
