<?php

namespace App\Services;

use App\Models\Article;
use App\Models\ArticleAttribute;
use App\Models\ArticleCategory;
use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\File;
use App\Models\FileReference;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WordpressImportService
{

    public static function import() {
        $items = self::loadAll();
    }

    public static function loadAll() {
        $results = [];
        $loading = true;
        $page = 1;
        $defaultUrl = "https://pharmablog.syntegon.com/feed/?page=";

        while($loading) {
            $url = $defaultUrl . $page;
            $rss = new \DOMDocument();
            $rss->load($url);

            foreach ($rss->getElementsByTagName('item') as $item) {

                $data = array (
                    'title' => $item->getElementsByTagName('title')->item(0)->nodeValue,
                    'desc' => $item->getElementsByTagName('description')->item(0)->nodeValue,
                    'link' => $item->getElementsByTagName('link')->item(0)->nodeValue,
                    'date' => $item->getElementsByTagName('pubDate')->item(0)->nodeValue,
                );
                dump($data);
                dd($item);
                echo "<a href='" . $item->link . "'>" . $item->title . "</a><br>";
                echo date("Y/m/d", strtotime($item->pubDate)) . "<br>";
            }

            /*
            if (count($response->content) === 0) {
                $loading = false;
            }*/

            // $results = array_merge($results, $response->content);

            $loading = false;
        }

        return $results;
    }


        // return \HubSpot::hubdb()->createTable('Dynamic Job Offers', $columns);

    /**
     * @param $url
     * @return array|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public static function get($url) {

    }
}
