<?php

namespace App\Http\Controllers;

use App\Jobs\CrawlHeliosJob;
use App\Jobs\CrawlJob;
use App\Jobs\CrawlSystemairJob;
use App\Jobs\GenerateHeliosAttributeJob;
use App\Jobs\CrawlSystemAirSitemapJob;
use App\Jobs\GenerateSystemAirAttributeJob;
use App\Models\CrawlResult;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    protected function import () {

        // step 1
        // lese alle helios urls von der datei resources/merk/helioslinks.csv
        $this->importHeliosUrls();

        // step 2
        // lese alle sitemaps von systemair ein
        $this->importSystemairSitemaps(1);

        // step 3
        // verarbeite unterschiedliche html codes
        CrawlSystemairJob::dispatch();
        CrawlHeliosJob::dispatch();

        // step 4
        // erstelle csv von allen attributen und speichere sie unter /resources/merk/export.csv
        $this->createCSV();
        /**
         * $crawlResult = CrawlResult::findOrFail(1373);
        GenerateHeliosAttributeJob::dispatchSync($crawlResult);
         */

        // $url = 'https://shop.systemair.com/de-DE/cl--linea--eco--bb1000wl--hor/p100012';
        // $url = 'https://heliosselect.de/cms/front_content.php?idart=21&idcat=17&artnr=2';

        // CrawlHeliosJob::dispatchSync($url, false);
        // CrawlSystemairJob::dispatchSync($url, false);
        // $this->importHelios();
        // $this->importSystemairSitemaps();
        // $this->createCSV();


        // $this->importSystemairSitemaps(1);
        // $this->importHeliosUrls();
        // CrawlHeliosJob::dispatch(false);

        // GenerateSystemAirAttributeJob::dispatchSync(CrawlResult::findOrFail(21));

    }

    protected function importSystemairSitemaps($pages = 1) { // aktuell hat es 138
        CrawlSystemAirSitemapJob::dispatch($pages);
    }

    protected function importHeliosUrls() {
        $heliosLinks = self::getFromCsvFile(resource_path('merk/helioslinks.csv'), true);

        foreach ($heliosLinks as $link) {
            $url = array_values($link)[0];
            CrawlJob::dispatch($url, false);
        }
    }

    protected function readCsv() {
        $csv = self::getFromCsvFile(resource_path('merk/jtl-export.csv'));
        dd($csv);
    }

    protected function createCSV() {
        $data = [
            "Artikelname" => "Ruck VM 125",
            "HAN" => "102647",
            "Bild" => "https://www.ruck.eu/pub/media/catalog/product/m/y/mymrv_ob_03_i_13.png",
            "Kurzbeschreibung" => "Ruck Zubehörteil 'VM 125' zur Verwendung mit Ruck Ventilationssystemen",
            "Beschreibung" => "",
            "SEO Name (Suchmaschinenname)" => "ruck vm 125",
            "SEO Titel-Tag" => "Ruck VM 125",
            "SEO Meta-Keywords" => "",
            "SEO Meta-Description" => "Ruck Zubehörteil 'VM 125' zur Verwendung mit Ruck Ventilationssystemen",
            "Std VK Netto" => "12,2",
            "EK Netto (für GLD)" => "5,856",
            "UVP" => "14,518",
            "Beschaffungszeit manuell ermitteln (Tage)" => "2",
            "Artikelgewicht" => "5",
            "Versandgewicht" => "5",
            "Versandklasse" => "Gewicht",
            "Hersteller" => "Ruck",
            "Kategorie Level 1" => "Neuanlage",
            "Kategorie Level 2" => "Ruck Zubehör für Ventilatoren",
            "Kategorie Level 3" => "Ruck Montagezubehör",
            "Kategorie Level 4" => "",
            "Kategorie Level 5" => "",
            "Kategorie Level 6" => "",
            "Kategorie Level 7" => "",
            "Shop lütungsmarkt.de Webshop aktiv" => "n",
            "Lieferant" => "Ruck Ventilatoren GmbH",
            "Lieferanten-Art.Nr." => "102647",
            "Lieferanten Artikelname" => "VM 125",
            "EK Netto" => "5,856",
            "Ist Dropshippingartikel" => "1",
            "Ist Standardlieferant" => "1",
            "mit Lagerbestand arbeiten" => "1",
            "Überverkäufe ermöglichen" => "1"
        ];


        $products = CrawlResult::whereNotNull('attributes')->all();

        $tempFileName = resource_path('merk/export.csv');
        $tempFile = fopen($tempFileName, 'w');

        fputcsv($tempFile, array_keys($data), ';');

        foreach ($products as $product) {

            $articleName = $product->attributes['title'];
            $article_no = $product->attributes['article_no'];
            $category1 = '';
            $image = $product->attributes['image'];

            if (strpos($product->url, 'systemair') > 0) {
                $category1 = 'Systemair';
                $vendor = 'Systemair';
                $shortDescrition = $product->attributes['description'];
                $descrition = $product->attributes['description'];
                $metaDescription = $product->attributes['description'];
            }
            elseif (strpos($product->url, 'heliosselect') > 0) {
                $category1 = 'Helios';
                $vendor = 'Helios';
                $articleName = 'HELIOS ' . $product->attributes['title'];
                $description = '';
                $shortDescrition = $product->attributes['description'];
                $metaDescription = $product->attributes['description'];
                $image = str_replace('/thumbs_detail/', '/', $product->attributes['image']);
            } else {
                throw new NotFoundHttpException('not found händler');
            }

            $data = [
                "Artikelname" => $articleName,
                "HAN" => $article_no,
                "Bild" => $image,
                "Kurzbeschreibung" => $shortDescrition,
                "Beschreibung" => $descrition,
                "SEO Name (Suchmaschinenname)" => Str::slug(key_exists('main_title',$product->attributes) ? $product->attributes['main_title'] : $product->attributes['title']),
                "SEO Titel-Tag" => $articleName,
                "SEO Meta-Keywords" => "",
                "SEO Meta-Description" => $metaDescription,
                "Std VK Netto" => "",
                "EK Netto (für GLD)" => "",
                "UVP" => "",
                "Beschaffungszeit manuell ermitteln (Tage)" => "2",
                "Artikelgewicht" => "",
                "Versandgewicht" => "",
                "Versandklasse" => "Gewicht",
                "Hersteller" => $category1,
                "Kategorie Level 1" => "Neuanlage",
                "Kategorie Level 2" => $category1,
                "Kategorie Level 3" => "",
                "Kategorie Level 4" => "",
                "Kategorie Level 5" => "",
                "Kategorie Level 6" => "",
                "Kategorie Level 7" => "",
                "Shop lütungsmarkt.de Webshop aktiv" => "n",
                "Lieferant" => $vendor,
                "Lieferanten-Art.Nr." => $article_no,
                "Lieferanten Artikelname" => key_exists('main_title',$product->attributes) ? $product->attributes['main_title'] : $product->attributes['title'],
                "EK Netto" => "",
                "Ist Dropshippingartikel" => "1",
                "Ist Standardlieferant" => "1",
                "mit Lagerbestand arbeiten" => "1",
                "Überverkäufe ermöglichen" => "1"
            ];

            fputcsv($tempFile, array_values($data), ';');
        }

        fclose($tempFile);
    }

    public static function getFromCsvFile($file, $rep = false)
    {
        $rows   = array_map(function($data) {
            return str_getcsv($data,";");
        }, file($file));
        $header = array_shift($rows);
        foreach ($header as $key => $value) {
            if ($rep) $header[$key] = strtoupper(trim(preg_replace('/[^A-Za-z0-9\-]/', '', $value)));
            else $header[$key] = $value;
        }

        $csv    = array();
        foreach($rows as $row) {
            try {
                $csv[] = array_combine($header, $row);
            } catch (\Exception $e) {
                throw $e;
                dd($row);
            }

        }
        return $csv;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->import();
    }

}
