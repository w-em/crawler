<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$frontendDomain = \Illuminate\Support\Facades\Config::get('app.app_url');
$backendDomain = \Illuminate\Support\Facades\Config::get('app.backend_url');

Route::get('/test', [App\Http\Controllers\TestController::class, 'index'])->where('path', '.*');
