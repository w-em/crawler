<?php

// use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
// use App\Http\Controllers\Auth\RegisterController;
// use App\Http\Controllers\Auth\ResetPasswordController;
// use App\Http\Controllers\Auth\VerificationController;

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AttributeController;
use App\Http\Controllers\AttributeGroupController;
use App\Http\Controllers\DirectoryController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\UserController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('logout', [LoginController::class, 'logout']);
    Route::post('refresh', [LoginController::class, 'refresh']);
    Route::get('me', [LoginController::class, 'me']);
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'users'
], function () {
    Route::get('/', [UserController::class, 'index']);
    Route::post('/', [UserController::class, 'create']);
    Route::get('/{id}', [UserController::class, 'show'])->where('id', '[0-9]+');
    Route::put('/{id}', [UserController::class, 'update'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'articles'
], function () {
    Route::get('/', [ArticleController::class, 'index']);
    Route::post('/', [ArticleController::class, 'create']);
    Route::get('/filtersByToken', [ArticleController::class, 'filtersByToken']);
    Route::get('/filters', [ArticleController::class, 'filters']);
    Route::post('/search', [ArticleController::class, 'search']);
    // Route::get('/search', [ArticleController::class, 'search']);
    Route::get('/{id}', [ArticleController::class, 'show'])->where('id', '[0-9]+');
    Route::get('/slug/{slug}', [ArticleController::class, 'showBySlug'])->where('slug', '.*');
    Route::get('/slugs/{slugs}', [ArticleController::class, 'showBySlugs'])->where('slugs', '.*');
    Route::put('/{id}', [ArticleController::class, 'update'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'attributegroups'
], function () {
    Route::get('/', [AttributeGroupController::class, 'index']);
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'attributes'
], function () {
    Route::get('/', [AttributeController::class, 'index']);
    Route::get('/{id}', [AttributeController::class, 'show'])->where('id', '[0-9]+');
    Route::put('/{id}', [AttributeController::class, 'update'])->where('id', '[0-9]+');
    Route::post('/', [AttributeController::class, 'create']);
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'categories'
], function () {
    Route::get('/', [CategoryController::class, 'index']);
    Route::get('/tree', [CategoryController::class, 'tree']);
    Route::get('/{id}', [CategoryController::class, 'show'])->where('id', '[0-9]+');
    Route::put('/{id}', [CategoryController::class, 'update'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'directories'
], function () {
    Route::get('/', [DirectoryController::class, 'index']);
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'files'
], function () {
    Route::get('/', [FileController::class, 'index']);
});

Route::get('/api/{path}', function () {
    abort(404);
})->where('path', '(.*)');
