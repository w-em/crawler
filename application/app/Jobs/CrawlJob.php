<?php

namespace App\Jobs;

use App\Models\CrawlResult;
use PHPHtmlParser\Dom;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CrawlJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 180;

    /**
     * @var null
     */
    protected $url = null;
    protected $data = null;

    /**
     * @var bool
     */
    protected $force = null;

    protected $contentMarkers = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $force = false)
    {
        $this->url = $url;
        $this->force = $force;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $count = CrawlResult::where([
            ['url', '=', $this->url]
        ])->count();

        if ($this->force || $count === 0) {
            $content = file_get_contents($this->url);

            CrawlResult::updateOrCreate([
                'url' => $this->url,
            ], [
                'html_content' => $content
            ]);
        }



    }
}
